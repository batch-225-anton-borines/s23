console.log('Pokemon Game')

/*
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
*/

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(i) {
		console.log(this.pokemon[i] + ", I choose you!")
	} 
}

console.log(trainer);

/*
6. Access the trainer object properties using dot and square bracket notation.
*/

console.log("Result of dot notation: ");
console.log(trainer.name);

console.log("Result of square bracket notation: ");
console.log(trainer['pokemon']);

/*
7. Invoke/call the trainer talk object method.
*/
console.log("Result of talk method: ");
trainer.talk(0);

/*
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
*/

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

/*

10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
*/

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        if(target.health <= 0) {
        	target.faint();
        }
        };
    this.faint = function(){
        console.log(this.name + ' fainted.');
    }
}

// Providing the object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(geodude);
console.log(geodude);
mewtwo.tackle(pikachu);
console.log(pikachu);